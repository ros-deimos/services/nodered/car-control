# Node-Red Car control

Car control is node-red flow who make a logical switch between many `ROS` topic controlled by HTTP API.

`ROS` topic switch for `/pwm/steering` :
* `/calculator/pwm/steering`
* `/websocket/pwm/steering`

`ROS` topic switch for `/pwm/speed` :
* `/calculator/pwm/speed`
* `/websocket/pwm/speed`

![Preview](img/flows.png)


## Control state

* Manual mode: control the Deimos car with gamepad from the browser 
* Calculator mode: control the Deimos car with the calculator service based on the video stream


## TODO

* Send data to DB


## Usage and Development
You can find this informations in some git repositories of the Deimos project
* For usage with the Deimos car, please check this [group of git repositories](https://gitlab.com/ros-deimos/docker-compose-architectures/with-hardware). In this, you can find docker-compose stacks of the car's stack and the dashboard stack.

* For development, please check this [git repositorie](https://gitlab.com/ros-deimos/docker-compose-architectures/dev/deimos-dashboard). In this, you can find docker-compose stacks of the dashboard stack with integrated `ROS` publishers of speed, direction and image recorded data.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)