FROM registry.gitlab.com/ros-deimos/docker-images/rcvnode:1.0

# Add node-red user so we aren't running as root.
RUN adduser --system --home /usr/src/node-red node-red


WORKDIR /usr/src/node-red

# Environment variable holding file path for flows configuration
ENV FLOWS=flows.json

ADD . .

RUN chown -R node-red /usr/src/node-red && \
    npm install && npm list --depth=0

USER node-red

CMD [ "npm", "start" ]
EXPOSE 1880

